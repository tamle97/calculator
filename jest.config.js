// jest.config.js
module.exports = {
  reporters: [
    'default',
    '<rootDir>/src/my-custom-reporter.js'
    // '<rootDir>/index.dev.js',
  ],
  moduleNameMapper: {
    "\\.(css|less|scss)$": "identity-obj-proxy"
  }
}
