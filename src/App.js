import React, { useState } from 'react';
import './App.css';
import * as CalculateFunc from './function';

const App = () => {
  const [result, setResult] = useState("");

  return (
    <div className="App">
      <h1>My Calculator</h1>
      <table border="1">
        <tbody>
          <tr>
            <td colSpan="3"><span>{result}</span></td>
            <td><input type="button" defaultValue="Clear" onClick={() => setResult(CalculateFunc.clr())} /> </td>
          </tr>
          <tr>
            <td><input type="button" defaultValue="1" onClick={() => setResult(CalculateFunc.dis(result, '1'))} /> </td>
            <td><input type="button" defaultValue="2" onClick={() => setResult(CalculateFunc.dis(result, '2'))} /> </td>
            <td><input type="button" defaultValue="3" onClick={() => setResult(CalculateFunc.dis(result, '3'))} /> </td>
            <td><input type="button" defaultValue="/" onClick={() => setResult(CalculateFunc.dis(result, '/'))} /> </td>
          </tr>
          <tr>
            <td><input type="button" defaultValue="4" onClick={() => setResult(CalculateFunc.dis(result, '4'))} /> </td>
            <td><input type="button" defaultValue="5" onClick={() => setResult(CalculateFunc.dis(result, '5'))} /> </td>
            <td><input type="button" defaultValue="6" onClick={() => setResult(CalculateFunc.dis(result, '6'))} /> </td>
            <td><input type="button" defaultValue="-" onClick={() => setResult(CalculateFunc.dis(result, '-'))} /> </td>
          </tr>
          <tr>
            <td><input type="button" defaultValue="7" onClick={() => setResult(CalculateFunc.dis(result, '7'))} /> </td>
            <td><input type="button" defaultValue="8" onClick={() => setResult(CalculateFunc.dis(result, '8'))} /> </td>
            <td><input type="button" defaultValue="9" onClick={() => setResult(CalculateFunc.dis(result, '9'))} /> </td>
            <td><input type="button" defaultValue="+" onClick={() => setResult(CalculateFunc.dis(result, '+'))} /> </td>
          </tr>
          <tr>
            <td><input type="button" defaultValue="." onClick={() => setResult(CalculateFunc.dis(result, '.'))} /> </td>
            <td><input type="button" defaultValue="0" onClick={() => setResult(CalculateFunc.dis(result, '0'))} /> </td>
            <td><input type="button" defaultValue="=" onClick={() => setResult(CalculateFunc.solve(result))} /> </td>
            <td><input type="button" defaultValue="*" onClick={() => setResult(CalculateFunc.dis(result, '*'))} /> </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
