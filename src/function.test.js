
import App from './App';
import React from 'react'
import TestRenderer from 'react-test-renderer';

import  { dis, clr, solve } from './function';


it('App component should rendered correctly', () => {
  const component = TestRenderer.create(
    <App></App>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it('Says My Calculator in the h1', () => {
  const component = TestRenderer.create(<App />);
  expect(component.root.findByType('h1').children).toEqual(['My Calculator']);
});


it('App component has 17 button', () => {
  const component = TestRenderer.create(<App />);
  expect(component.root.findAllByType('input').length).toEqual(17);});

it('fail', () => {
  const actual = dis("12", "3");
  const expected = "123";
  expect(actual).toBe(expected);
});

it('Test clear',()=>
{
const num = dis("1","2");
const exp = clr("num");
expect(exp).toBe("");
});

it('Test number',()=>
{
const num = dis("","2");
expect(num).toBe("2");
});

it('Test float',()=>
{
const num = dis("1.","2");
expect(num).toBe("1.2");
});

it('Test Plus',()=>
{
const num = dis("1+","2");
const sum = solve(num)
expect(sum).toBe(3);
});

it('Test Minus',()=>
{
const num = dis("5-","2");
const sum = solve(num)
expect(sum).toBe(3);
});

it('Test Div',()=>
{
const num = dis("1/","2");
const sum = solve(num)
expect(sum).toBe(0.5);
});

it('Test Mul',()=>
{
const num = dis("1*","2");
const sum = solve(num)
expect(sum).toBe(2);

});
