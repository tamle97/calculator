var exec = require('child_process').exec;
var nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true, // upgrade later with STARTTLS
  auth: {
    user: "bot.sendmail97@gmail.com",
    pass: "tamle123"
  }
});

const mailOptions = (toMail, failedNumber, time) => {
  return {
    from: 'bot.sendmail97@gmail.com',
    to: toMail,
    subject: '[NOTIFICATION-Error test case]',
    text: `New pushing has ${failedNumber} error Test Case!\n
          Time: ${time}`
  }
};

const teamateMails = [
  "lethanhtam319106@gmail.com",
  "tieuthuong61@gmail.com",
  "hoanglong16198@gmail.com"
]


// my-custom-reporter.js
class MyCustomReporter {
  constructor(globalConfig, options) {
    this._globalConfig = globalConfig;
    this._options = options;
  }

  onRunComplete(contexts, results) {
    console.log('Custom reporter output:', results);
    if (results.numFailedTests > 0) {
      const date = new Date(results.startTime);
      const formatTime = date.toDateString() + " - " + date.toLocaleTimeString();

      exec(`curl -X POST -H 'Content-type: application/json' --data '{"text":"New pushing has ${results.numFailedTests} failed Test Case!\nTime: ${formatTime}"}' https://hooks.slack.com/services/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`);
      teamateMails.map(mail => {
        transporter.sendMail(mailOptions(mail, results.numFailedTests, formatTime), function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log('Email sent: ' + info.response);
          }
        });
      })
    }
  }

  getLastError() {
    if (this._shouldFail) {
      return new Error('my-custom-reporter.js reported an error');
    }
  }
}

module.exports = MyCustomReporter;
  // or export default MyCustomReporter;